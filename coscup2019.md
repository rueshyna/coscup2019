slidenumbers: true
footer: Tezos Southeast Asia X 9 Chapters

# 懶惰鬼的函數式爬蟲
## 以 Tezos 應用需求為例

### COSCUP 2019

---

# Who am I

## ChiaChi Tsai

### Haskell Programmer
### of Nine Chapters

#### Assist TSA to support Tezos ecosystem

![right fit](./img/9_logo.png)

---

### Tezos Southeast Asia(TSA) 是獨立運行的非營利組織，致力於在東南亞推廣 *Tezos* 區塊鏈。Tezos 是世界頂尖的區塊鏈技術：提供高度安全性的智慧合約和分散應用平台，並具有「自我修正協定」、「鍊上治理」以及「易於形式驗證的智慧合約語言 - Michelson」。

![right fit](./img/tsa_log.png)

---

- 函數式 (Functional / Haskell)
- 區塊鏈 (Blockchain / Tezos)
- 一般化 (general) 的 JSON 爬蟲

---

# __軟體開發源自於__
# 工程師懶惰的
# 需求

---

# __軟體開發源自於__
# 老闆的需求

---

# __軟體開發源自於__
# 工程師懶惰+
# 老闆的需求

---

# 老闆的需求
- Crawl/Index __Tezos__
- __Tezos__ → 第三方服務
  - 提供 JSON RPC

![right fit](./img/tezos_rpc_sample.png)

---

# 市面第三方服務的共同特色…

- 大多都有提供 JSON API
  - e.g. Twitter, AWS, etc

---

# 市面第三方服務的共同特色…

- 大多都有提供 JSON API
  - e.g. Twitter, AWS, etc
- 妳/你可能遇過下面這些困擾
  - 每次/每個時間拿到的 JSON 有時候會和預期的不一樣
     - 可能該服務改版本等原因
  - 已經寫過上百有針對某 JSON 服務的爬蟲/剖析器

---

# 總是千篇一律

1. 給定路徑
1. 抓出該路徑的值
1. 存入 DB 或某地方
1. 應用分析爬回來的資料

![left fit](./img/tezos_rpc_sample.png)

---

# 實際第三方服務的例子

---

# Tezos / 區塊鏈

- 區塊鏈 ： 區塊一個接一個
- 1 分鐘產生一個新的區塊
- 提供 JSON 格式的 API 描述區塊的資訊

稍微更深入的瞭解 Tezos 一點 ...

---

# Tezos 三大特色之一
## 共識演算法 - Liquid Proof-of-Stake

  - 動態決定驗証者/建區塊候選人 (可多達上萬人)
  - 促進 token 持有者協同合作管理 Tezos

---

# Tezos 三大特色之二
## 形式化驗証 (Formal verification)

- Functional programming language 提供 formal method 的基礎
- 數學証明
- 保証系統安全性

---

# Tezos 三大特色之三
## 鏈上治理 (On chain government)

- 可透過__投票__機制更改協定
  - 也就是說 JSON 格式可能更改
  - 第 999 區塊和第 2019 個可能 JSON 不一樣
     - 造成應用程式需要修改

---

# 一勞永逸方法！？

- 是否路徑可以是設定檔的一部份 ？
- 是否隨時監測第三方服務的格式 ？
- 是否更進一步協助管理資料庫或適應格式改變 ？

---

# Baking Soda[^1]

- A general crawler/indexer
- For any services in JSON format
- Written in Haskell

![right 80%](./img/bakingsoda_logo.png)

[^1]: https://gitlab.com/9chapters/baking-soda

---

# Baking Soda

> Because it has long been known and is widely used
-- from Wikipedia

![right 80%](./img/bakingsoda_logo.png)

---

# Why Haskell?

---

# Domain Specific Language (DSL)

- 應用在特定的情境的小語言
   - 資料是本體
   - 描述如何操作資料

目的: 精簡的語法，更能夠專注在程式邏輯本身

---

# DSL

- 例如 SQL
  - Database 視為是資料結構
  - SQL 操作資料結構的方法

---

# The "domain specific"

回到 Baking Soda -- JSON data indexer

- 針對特定範圍：domain specific
- domain-driven development
    - 對「特定」的資料 - JSON
    - 做「特定」的處理 - Crawling/Parsing/Indexing

---

# Why DSL in Haskell

- purely functional
- lazy
- strong type
- type inference
- point free
- precise describe data
- etc

---

## 通常寫 Haskell 的第一步
## 定義好資料結構

---

# Algebraic Data Type(ADT) Driven

- 清晰語義
- 數學性質

---

# 二元樹

```haskell
data Tree a
  = Branch (Tree a) (Tree a)
  | Leaf a
```

---

# 二元樹

```haskell
data Tree a
  = Branch (Tree a) (Tree a)
  | Leaf a
```

- Induction
  - Recursion rule
  - Base case (termination)

---

```haskell
-- 資料
Branch
  (Branch (Leaf 1) (Leaf 2))
  (Leaf 3)
```

---

```haskell
data Tree a
  = Branch (Tree a) (Tree a)
  | Leaf a

deal (Branch l r) = ...
deal (Leaf f) = ...
```

---

# Formal Program Synthesis

- 因為其 strong type 和 ADT 的特性

直接看例子 Deriving

---
# Formal Program Synthesis

- Java code

```java
class Student {
  int no;
  String name;

  Student(int no, String name) {...}

  public String toString() {
    return no + " " + name;
  }

  public static void main(String args[]) {
    Student s = new Student(101, "Walter");
    Sytem.out.println(s);
  }
}

```
---
# Formal Program Synthesis

- Java code

[.code-highlight: 7-9]
```java
class Student {
  int no;
  String name;

  Student(int no, String name) {...}

  public String toString() {
    return no + " " + name;
  }

  public static void main(String args[]) {
    Student s = new Student(101, "Walter");
    Sytem.out.println(s);
  }
}

```
---

- Java code
  - override
  - 無法順著結構產生 code


---

# Formal Program Synthesis

- Haskell code

```haskell
module Main where

data Student = Student
  { no   :: Int
  , name :: String
  }
  deriving (Show)

main = print (Student 101 "Walter")
```

---

# Formal Program Synthesis

- Haskell code

[.code-highlight: 7]
```haskell
module Main where

data Student = Student
  { no   :: Int
  , name :: String
  }
  deriving (Show)

main = print (Student 101 "Walter")
```

---

# Formal Program Synthesis

- Deriving
  - Show, Read, Eq, etc
  - Show, Read 是寫簡易設定檔的好幫手！

---

# Easy to Parallelism

- referential transparency
- no side effect

e.g 
X = 1 → X 永遠是 1

---

# Easy to Parallelism

因為 referential transparency

- divide and conquer
- 大的問題拆成小的問題
- 平行處理小問題
- 不需擔心 race condition

---

# Easy to Parallelism

- 物盡其用 - 善用你的核心數量

範例：

```haskell
-- N 階層
fac 0 = 1
fac n = n * fac (n - 1)

-- 費式數列
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)
```

---

```haskell
-- single core
main = print (a + b)
  where a = fac 42
        b = fib 34

-- multiple core
main = a `par` b `pseq` print (a + b)
  where a = fac 42
        b = fib 34
```

---

# Baking Soda

![right 80%](./img/bakingsoda_logo.png)

---

# Baking Soda 的 3 重點

- Crawler
- Analyser
- Indexer

---

**Architecture of Baking Soda**

![original fit](./img/baking_soda_architecture.png)

---

# Crawler

- 存原始檔案
  - 永遠不要相信第三方服務
  - 避免不小心為對方做壓力測試
  - 利於後續批次處理

---

# 如何爬？

- Seed
- Next Step

找出可以接續的方法

---

# 以 Tezos 為例

```html
/chain/main/blocks/1
/chain/main/blocks/2
/chain/main/blocks/3
/chain/main/blocks/4
...
→ /chain/main/blocks/$block.level$
```

---

```haskell
data TzBlock = TB { level :: Int }

class Crawler a where
  seed     :: a
  nextStep :: a -> a

instance Crawler TzBlock where
  seed        = TB 0
  nextStep tb = tb { level = level tb + 1 }
```

---

# Analyser

- JSON 的 23 事…
  - 通常第三方服務不太會提供 schema
     - 難以看到全局
  - 結構改變
     - 有可能無法向後相容

---

```html
<!-- version 1 -->
{
  "error": {
    "message": "something wrong"
  }
}
<!-- version 2 -->
{
  "error": {
    "message": [
        {"lang": "en", "something wrong"
        ,"lang": "zh-tw", "錯誤"
        }
  }
}
```
- message 的值從 type string 變成 type array

---

# 分析什麼東西？

- 分析 Crawler 爬回來的 JSON
  - 列出每條路徑，取聯集。 e.g.
    - header > level
    - header > fitness > 0
    - ...

![right fit](./img/tezos_rpc_sample.png)

---

# 分析什麼東西？

- 可能還會需要
  - Stop Rule
  - Reverse Index

---

# 分析什麼東西？

- 分析末端結點的值
  - 產生對應的 SQL schema

| JSON Type | SQL TYPE |
| --- | --- |
| Number | floating-point numbers or integers |
| String | varchar, text or time data types |

---

# Config

- 描述目標值
    - 路徑
    - 儲存型態
    - 目標表格
    - 目標欄位
    - 相關 SQL 設定

![right fit](./img/config_sample.png)

---

# Indexer

- 依設定所描述的路徑
  - 取得目標值
  - 存入資料庫

---

# Indexer

- 容易擴張 ( scaling )
  - 設定檔可以拆小
  - 起多個 processes

![right fit](./img/config_sample.png)

---

# 效能比較

- ~ 500000 blocks
- Tzscan[^2], Nomadic Indexer[^3]
  - 具有針對性爬蟲程式
  - 只適用於 Tezos

[^2]: Tzscan: https://gitlab.com/tzscan/tzscan

[^3]: Nomadic Indexer: https://gitlab.com/nomadic-labs/tezos-indexer

---
# 效能比較

## ~ 500000 blocks

| Program | Time |
| --- | --- |
| Baking Soda | ~ 13 hours |
| Tzscan      | ~ 5 days |
| Nomadic Indexer | ~ 3 days |

---

# 資料庫的選擇
### general or specific ？

- Haskell 本身的特性
  - type strong and ADT
  - easy to provide universal data store interface
- Lib
  - haskell-persistent / haskell-groundhog

---

# 資料庫的選擇

- General
  - 使用者可任選資料庫
  - 放棄某資料庫特有的功能. e.g. Postgres: array type
- Specific
  - 比較好的使用效率

---

# Apply to Tezos case

~ 500000 blocks

| Step | Time |
| --- | --- |
| Crawl | 2~3 hours |
| Analysis |  blocks 1~2 hours |
| Index | ~ 13 hours |

---

# Apply to Tezos case

- Config 產生約 7000 多行
  - 綜觀所有區塊歷史的痕跡
     - 改名: managerPubkey → manager_pubkey
     - 錯字更正: level → cycle
     - 結構改變

---

#  有關開發的目前狀況

- 釋出 alphanet 版本
- 更新公告: https://9chapters.gitlab.io/devlog/

---

#  有關應用的目前狀況

- Baking Soda 的二個 use cases
  - Tezos
  - MyTezosBaker[^4]
- Baking Soda 的下游應用
  - Tezos.ID 的 block explorer

[^4]: https://mytezosbaker.com/

---

![](./img/tezos_id.png)

---

# Thanks! Question?
![](./img/tsa_log.png)
![80%](./img/bakingsoda_logo.png)
![](./img/9_logo.png)
